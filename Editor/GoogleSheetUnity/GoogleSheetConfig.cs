using Google.Apis.Auth.OAuth2;
using Google.Apis.Services;
using Google.Apis.Sheets.v4;
using Google.Apis.Sheets.v4.Data;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

[CreateAssetMenu]
public class GoogleSheetConfig : ScriptableObject
{
    public TextAsset Json;
    public string SpreadsheetID;
    public string[] Scopes => new[] { SheetsService.Scope.Spreadsheets };
    Spreadsheet _spreadsheet;
    Spreadsheet Spreadsheet => _spreadsheet ?? (_spreadsheet = GetSpreadsheet());
    SheetsService _service;
    SheetsService Service => _service ?? (_service = GetService());
    SheetsService GetService()
    {
        string file = Json.name + ".json";
        if (!File.Exists(file))
        {
            File.WriteAllText(file, Json.text);
        }

        GoogleCredential credential;
        using (var stream = new FileStream(file, FileMode.Open, FileAccess.Read))
        {
            credential = GoogleCredential.FromStream(stream)
                .CreateScoped(Scopes);
        }
        return new SheetsService(new BaseClientService.Initializer()
        {
            ApplicationName = "JK",
            HttpClientInitializer = credential
        });
    }
    public Spreadsheet GetSpreadsheet()
    {
        Spreadsheet spreadSheetData = Service.Spreadsheets.Get(SpreadsheetID).Execute();
        return spreadSheetData;
    }
    [ContextMenu("Get Data")]
    void GetData()
    {
        var Sheets = new List<string>();
        foreach (var item in Spreadsheet.Sheets)
        {
            Sheets.Add(item.Properties.Title);
        }
        SpreadsheetsResource.ValuesResource.BatchGetRequest request = Service.Spreadsheets.Values.BatchGet(SpreadsheetID);
        request.Ranges = Sheets;
        BatchGetValuesResponse response = request.Execute();
        foreach (ValueRange valueRange in response.ValueRanges)
        {
            Debug.Log(valueRange.Range);
            foreach (var item in valueRange.Values)
            {
                Debug.Log(string.Join(",", item));
            }
        }
    }

    [ContextMenu("Add Sheet")]

    void TestAddSheet()
    {
        string sheetName = "Ads";
        foreach (var item in GetSpreadsheet().Sheets)
        {
            Debug.Log(item.ToString());
        }
        return;
        var addSheetRequest = new AddSheetRequest();
        addSheetRequest.Properties = new SheetProperties();
        addSheetRequest.Properties.Title = sheetName;
        BatchUpdateSpreadsheetRequest batchUpdateSpreadsheetRequest = new BatchUpdateSpreadsheetRequest();
        batchUpdateSpreadsheetRequest.Requests = new List<Request>();
        batchUpdateSpreadsheetRequest.Requests.Add(new Request
        {
            AddSheet = addSheetRequest
        });

        var batchUpdateRequest =
            Service.Spreadsheets.BatchUpdate(batchUpdateSpreadsheetRequest, SpreadsheetID);

        batchUpdateRequest.Execute();
        Debug.Log("Add Done!");
    }

    void CreateSheet(string sheetName)
    {
        var addSheetRequest = new AddSheetRequest();
        addSheetRequest.Properties = new SheetProperties();
        addSheetRequest.Properties.Title = sheetName;
        BatchUpdateSpreadsheetRequest batchUpdateSpreadsheetRequest = new BatchUpdateSpreadsheetRequest();
        batchUpdateSpreadsheetRequest.Requests = new List<Request>();
        batchUpdateSpreadsheetRequest.Requests.Add(new Request
        {
            AddSheet = addSheetRequest
        });

        var batchUpdateRequest =
            Service.Spreadsheets.BatchUpdate(batchUpdateSpreadsheetRequest, SpreadsheetID);

        batchUpdateRequest.Execute();
        Debug.Log("Create sheet " + sheetName + "done!");
    }
    void AddSheet(string sheetName)
    {
        var range = $"{sheetName}!A1:Z1000";
        var valueRange = new ValueRange();
        var oblist = new List<object>() { "Hello!", "This", "was", "insertd", "via", "C#" };
        valueRange.Values = new List<IList<object>> { oblist };
        var appendRequest = Service.Spreadsheets.Values.Append(valueRange, SpreadsheetID, range);
        appendRequest.ValueInputOption = SpreadsheetsResource.ValuesResource.AppendRequest.ValueInputOptionEnum.USERENTERED;
        var appendReponse = appendRequest.Execute();
    }
}
