Create a service account: https://console.developers.google.com/iam-admin/serviceaccounts/
In options create a key: this key is your usual client_secret.json - use it the same way
Make the role owner for the service account Member name = service account ID = service account email ex: joukyuu@api-game-410104.iam.gserviceaccount.com
Copy the email address of your service account = service account ID
Simply go in your browser to the Google sheet you want to interact with
Go to SHARE on the top right of your screen
Go to advanced settings and share it with an email address of your service account ex: joukyuu@api-game-410104.iam.gserviceaccount.com